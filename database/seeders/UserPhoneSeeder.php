<?php

namespace Database\Seeders;

use App\Domains\User\Models\Phone;
use App\Domains\User\Models\User;
use Database\Seeders\Traits\DisableForeignKeys;
use Database\Seeders\Traits\TruncateTable;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserPhoneSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seed.
     */
    public function run()
    {
        Phone::factory(5)->create();
    }
}
