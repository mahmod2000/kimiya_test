<?php

namespace Database\Seeders;

use App\Domains\Property\Models\Property;
use App\Domains\User\Models\User;
use Database\Seeders\Traits\DisableForeignKeys;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class PropertySeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        Property::factory(10)->create();
    }
}
