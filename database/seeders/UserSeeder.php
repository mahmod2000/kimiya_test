<?php

namespace Database\Seeders;

use App\Domains\User\Models\Phone;
use App\Domains\User\Models\User;
use Database\Seeders\Traits\DisableForeignKeys;
use Database\Seeders\Traits\TruncateTable;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seed.
     */
    public function run()
    {
        User::factory(5)->create()->each(function ($s){
            $s->phones()->saveMany(Phone::factory(2)->create([
                'user_id' => $s->id
            ]));
        });
    }
}
