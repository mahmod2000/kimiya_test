<?php

namespace Tests\Feature;

use App\Domains\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PropertyTest extends TestCase
{
    protected $property_id;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_list_all_properties()
    {
        $response = $this->getJson('api/property', [
            'Accept' => 'application/json'
        ]);

        $response->assertJson([
            'success' => true,
        ]);
    }

    /**
     * Create a new property test
     */
    public function test_create_a_new_property()
    {
        $response = $this->create_item();

        $response->assertJson([
            'success' => true,
        ]);
    }

    public function test_update_a_new_property()
    {
        $response = $this->create_item();

        $data = json_decode($response->getContent());

        $property_id = 0;
        if($data->success === true) $property_id = $data->data->id;

        $data = $this->make_update_data();

        $response = $this->patchJson('api/property/'.$property_id, $data, [
            'Accept' => 'application/json'
        ]);

        $response->assertJson([
            'data' => ['id' => $property_id],
            'success' => true,
        ]);
    }

    /**
     * Show one property
     * @Test
     */
    public function test_show_one_property()
    {
        $response = $this->create_item();

        $data = json_decode($response->getContent());

        $property_id = 0;
        if($data->success === true) $property_id = $data->data->id;

        $response = $this->getJson('api/property/'.$property_id, [
            'Accept' => 'application/json'
        ]);

        $response->assertJson([
            'data' => ['id' => $property_id],
            'success' => true,
        ]);
    }

    public function test_remove_item()
    {
        $response = $this->create_item();
        $data = json_decode($response->getContent());
        $property_id = 0;
        if($data->success === true) $property_id = $data->data->id;

        $response = $this->deleteJson('api/property/'.$property_id, [
            'Accept' => 'application/json'
        ]);

        $response->assertJson([
            'data' => ['id' => $property_id],
            'success' => true,
        ]);
    }

    /**
     * Prepare create data
     * @return array
     */
    public function make_create_data()
    {
        $user = User::first();

        return [
            'name' => 'A new property is added by Test',
            'user_ids' => [$user->id],
            'house_name_number' => 'The address from Test api',
            'postcode' => 'The PostalCode from Test api',
        ];
    }

    /**
     * Prepare Update data
     * @return array
     */
    public function make_update_data()
    {
        $user = User::first();

        return [
            'name' => 'A new property is updated by Test',
            'user_ids' => [$user->id],
            'house_name_number' => 'The address from Test api updated',
            'postcode' => 'The PostalCode from Test api updated',
        ];
    }

    /**
     * Create item to using in all data
     * @return \Illuminate\Testing\TestResponse
     */
    public function create_item()
    {
        $data = $this->make_create_data();

        return $this->postJson('api/property', $data, [
            'Accept' => 'application/json'
        ]);
    }
}
