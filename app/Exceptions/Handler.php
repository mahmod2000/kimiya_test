<?php

namespace App\Exceptions;

use App\Helpers\MyResponseJson;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function render($request, $exception)
    {
        if ($exception instanceof ModelNotFoundException or $exception instanceof NotFoundHttpException) {
            return MyResponseJson::set([], $exception->getMessage(), false);
        } else if ($exception instanceof ValidationException) {
            return MyResponseJson::set($exception->errors(), $exception->getMessage(), false);
        }

        return parent::render($request, $exception);
    }
}
