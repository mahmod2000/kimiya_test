<?php

namespace App\Helpers;

class MyResponseJson
{
    /**
     * @param array $data
     * @param null $message
     * @param bool $success
     * @return \Illuminate\Http\JsonResponse
     */
    public static function set($data = [], $message = null, $success = true)
    {
        return response()->json([
            'data' => $data,
            'message' => $message,
            'success' => $success,
        ]);
    }
}
