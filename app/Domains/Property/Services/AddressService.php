<?php

namespace App\Domains\Property\Services;

use App\Domains\Property\Models\Address;
use App\Domains\Property\Models\Property;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;

class AddressService extends BaseService
{
    /**
     * AddressService constructor.
     * @param Address $address
     */
    public function __construct(Address $address)
    {
        $this->model = $address;
    }

    /**
     * @param Property $property
     * @param array $data
     * @return mixed
     * @throws GeneralException
     */
    public function createByProperty(Property $property, $data = [])
    {
        DB::beginTransaction();

        try {
            $item = $property->address()->create([
                'house_name_number' => $data['house_name_number'] ?? null,
                'postcode' => $data['postcode'] ?? null,
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            throw new GeneralException('There is a problem during adding address of property');
        }

        // TODO run an event

        DB::commit();

        return $item;
    }

    /**
     * @param Property $property
     * @param array $data
     * @return Address
     * @throws GeneralException
     */
    public function update(Property $property, $data = [])
    {
        DB::beginTransaction();

        try {
            $property->address()->update([
                'house_name_number' => $data['house_name_number'] ?? (optional($property->address)->house_name_number ?? null),
                'postcode' => $data['postcode'] ?? (optional($property->address)->postcode ?? null),
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            throw new GeneralException('There is a problem during updating address of property');
        }

        // TODO run an event

        DB::commit();

        return $property->address;
    }

    /**
     * @param Address $address
     * @return bool
     * @throws GeneralException
     * @throws \Exception
     */
    public function remove(Address $address)
    {
        if($this->deleteById($address->id)) {
            // TODO run an event

            return true;
        }

        throw new GeneralException('There is a problem to remove address of property');
    }
}
