<?php

namespace App\Domains\Property\Services;

use App\Domains\Property\Jobs\AddedNewPropertyJob;
use App\Domains\Property\Models\Property;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;

class PropertyService extends BaseService
{
    /**
     * @var AddressService
     */
    private $addressService;

    /**
     * PropertyService constructor.
     * @param Property $property
     * @param AddressService $addressService
     */
    public function __construct(Property $property,
                                AddressService $addressService)
    {
        $this->model = $property;
        $this->addressService = $addressService;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $this->with = ['address', 'owners'];
        return $this->all();
    }

    /**
     * @param array $data
     * @return mixed
     * @throws GeneralException
     */
    public function create($data = [])
    {
        DB::beginTransaction();

        try {
            $item = $this->model::create([
                'name' => $data['name'],
            ]);

            /** Making address from service */
            $this->addressService->createByProperty($item, $data);

            /** Syncing owners by user_ids --- ex: user_ids[0]=1 & user_ids[1]=1 */
            if(isset($data['user_ids']) and count($data['user_ids'])) $item->owners()->sync($data['user_ids']);

        } catch (\Exception $e) {
            DB::rollBack();

            throw new GeneralException('There is a problem during adding property. error: ' . $e->getMessage());
        }

        /** TODO run an event */
        // event(new PropertyCreated($item));

        /** TODO Run job */
        // dispatch(new AddedNewPropertyJob($item));

        DB::commit();

        $this->with = ['owners', 'address'];
        return $this->getById($item->id);
    }

    /**
     * @param Property $property
     * @param array $data
     * @return Property
     * @throws GeneralException
     */
    public function update(Property $property, $data = [])
    {
        DB::beginTransaction();

        try {
            $property->update([
                'name' => $data['name'],
            ]);

            /** Updating address from service */
            $this->addressService->update($property, $data);

            /** Syncing owners by user_ids --- ex: user_ids[0]=1 & user_ids[1]=1 */
            if(isset($data['user_ids']) and count($data['user_ids'])) $property->owners()->sync($data['user_ids']);

        } catch (\Exception $e) {
            DB::rollBack();

            throw new GeneralException('There is a problem during updating property.');
        }

        // TODO run an event

        DB::commit();

        $this->with = ['owners', 'address'];
        return $this->getById($property->id);
    }

    /**
     * @param Property $property
     * @return bool
     * @throws GeneralException
     * @throws \Exception
     */
    public function remove(Property $property)
    {
        DB::beginTransaction();

        /** Remove address */
        $property->address()->delete();

        /** Remove Owners */
        $property->owners()->detach();

        if($this->deleteById($property->id)) {
            // TODO run an event

            DB::commit();
            return true;
        }

        DB::rollBack();

        throw new GeneralException('There is a problem to remove property.');
    }

    /**
     * @param $property_id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getOneById($property_id)
    {
        $this->with = ['address', 'owners'];
        return $this->getById($property_id);
    }
}
