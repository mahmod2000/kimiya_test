<?php

namespace App\Domains\Property\Http\Controllers\Api;

use App\Domains\Property\Services\AddressService;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    /**
     * @var AddressService
     */
    private $addressService;

    public function __construct(AddressService $addressService)
    {
        $this->addressService = $addressService;
    }
}
