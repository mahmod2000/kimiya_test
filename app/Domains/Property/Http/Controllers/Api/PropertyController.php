<?php

namespace App\Domains\Property\Http\Controllers\Api;

use App\Domains\Property\Http\Requests\PropertyCreateRequest;
use App\Domains\Property\Http\Requests\PropertyUpdateRequest;
use App\Domains\Property\Http\Resources\Property\Property as PropertyResource;
use App\Domains\Property\Http\Resources\Property\PropertyCollection;
use App\Domains\Property\Models\Property;
use App\Domains\Property\Services\PropertyService;
use App\Helpers\MyResponseJson;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class PropertyController
 * @package App\Domains\Property\Http\Controllers\Api
 */
class PropertyController extends Controller
{
    /**
     * @var PropertyService
     */
    private $propertyService;

    /**
     * PropertyController constructor.
     * @param PropertyService $propertyService
     */
    public function __construct(PropertyService $propertyService)
    {
        $this->propertyService = $propertyService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $properties = $this->propertyService->index();

        return MyResponseJson::set(new PropertyCollection($properties));
    }

    /**
     * @param Property $property
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Property $property)
    {
        return MyResponseJson::set(new PropertyResource($this->propertyService->getOneById($property->id)));
    }

    /**
     * @param PropertyCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function store(PropertyCreateRequest $request)
    {
        $property = $this->propertyService->create($request->validated());

        return MyResponseJson::set(new PropertyResource($property));
    }

    /**
     * @param PropertyUpdateRequest $request
     * @param Property $property
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function update(PropertyUpdateRequest $request, Property $property)
    {
        $property = $this->propertyService->update($property, $request->validated());

        return MyResponseJson::set(new PropertyResource($property));
    }

    /**
     * @param Property $property
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Property $property)
    {
        $this->propertyService->remove($property);

        return MyResponseJson::set(new PropertyResource($property));
    }
}
