<?php

namespace App\Domains\Property\Http\Resources\Address;

use Illuminate\Http\Resources\Json\JsonResource;

class Address extends JsonResource
{
    public function toArray($request)
    {
        return [
            'house_name_number' => $this->house_name_number,
            'postcode' => $this->postcode,
        ];
    }
}
