<?php

namespace App\Domains\Property\Http\Resources\Property;

use App\Domains\Property\Http\Resources\Address\Address;
use App\Domains\User\Http\Resources\User\UserCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class Property extends JsonResource
{
    public function toArray($request)
    {
        return [
            'address' => new Address($this->whenLoaded('address')),
            'id' => $this->id,
            'owners' => new UserCollection($this->whenLoaded('owners')),
        ];
    }
}
