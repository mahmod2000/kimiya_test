<?php

namespace App\Domains\Property\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'user_ids' => 'nullable|array',
            'house_name_number' => 'nullable|max:191',
            'postcode' => 'nullable|max:191',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Property Name',
            'user_ids' => 'Property Name',
            'house_name_number' => 'Address House name number',
            'postcode' => 'Address Postal Code',
        ];
    }
}
