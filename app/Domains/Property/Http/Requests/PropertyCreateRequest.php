<?php

namespace App\Domains\Property\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'user_ids' => 'nullable|array',
            'house_name_number' => 'required|max:191',
            'postcode' => 'required|max:191',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Property Name',
            'user_ids' => 'Property Name',
            'house_name_number' => 'Address House name number',
            'postcode' => 'Address Postal Code',
        ];
    }
}
