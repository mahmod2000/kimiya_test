<?php

namespace App\Domains\Property\Models\Relations;

use App\Domains\Property\Models\Property;

trait AddressRelations
{
    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
