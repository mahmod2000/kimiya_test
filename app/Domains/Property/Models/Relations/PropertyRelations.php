<?php

namespace App\Domains\Property\Models\Relations;

use App\Domains\Property\Models\Address;
use App\Domains\User\Models\User;

trait PropertyRelations
{
    public function address()
    {
        return $this->hasOne(Address::class);
    }

    public function owners()
    {
        return $this->belongsToMany(User::class, 'property_owners');
    }
}
