<?php

namespace App\Domains\Property\Models;

use App\Domains\Property\Models\Relations\AddressRelations;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use AddressRelations;

    /** @var string */
    protected $table = 'property_address';

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'property_id',
        'house_name_number',
        'postcode',
    ];
}
