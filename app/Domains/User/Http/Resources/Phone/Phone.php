<?php

namespace App\Domains\User\Http\Resources\Phone;

use Illuminate\Http\Resources\Json\JsonResource;

class Phone extends JsonResource
{
    public function toArray($request)
    {
        return [
            'number' => $this->number,
            'type' => $this->type,
        ];
    }
}
