<?php

namespace App\Domains\User\Http\Resources\User;

use App\Domains\User\Http\Resources\Phone\PhoneCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    public function toArray($request)
    {
        return [
            'first_name' => $this->first_name,
            'id' => $this->id,
            'last_name' => $this->last_name ,
            'phones' => new PhoneCollection($this->phones),
        ];
    }
}
