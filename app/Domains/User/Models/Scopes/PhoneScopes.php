<?php

namespace App\Domains\User\Models\Scopes;

trait PhoneScopes
{
    /**
     * @param $query
     * @param $type
     * @return mixed
     */
    public function scopeByType($query, $type)
    {
        return $query->whereType($type);
    }
}
