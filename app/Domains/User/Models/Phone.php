<?php

namespace App\Domains\User\Models;

use App\Domains\User\Models\Relations\PhoneRelations;
use App\Domains\User\Models\Scopes\PhoneScopes;
use Database\Factories\UserPhoneFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    use PhoneRelations, PhoneScopes, HasFactory;

    const TYPE_MOBILE = 'mobile';
    const TYPE_HOME = 'home';
    const TYPE_WORK = 'work';

    protected $table = 'user_phones';
    protected $fillable = [
        'user_id',
        'type',
        'number',
    ];

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return UserPhoneFactory::new();
    }
}
