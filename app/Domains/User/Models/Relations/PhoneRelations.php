<?php

namespace App\Domains\User\Models\Relations;
use App\Domains\User\Models\User;

trait PhoneRelations
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
