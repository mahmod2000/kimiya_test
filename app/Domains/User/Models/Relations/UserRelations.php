<?php

namespace App\Domains\User\Models\Relations;
use App\Domains\Property\Models\Property;
use App\Domains\User\Models\Phone;

trait UserRelations
{
    public function phones()
    {
        return $this->hasMany(Phone::class);
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class, 'property_owners');
    }
}
