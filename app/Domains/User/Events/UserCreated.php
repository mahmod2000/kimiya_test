<?php

namespace App\Domains\User\Events;

use Illuminate\Queue\SerializesModels;

class UserCreated
{
    use SerializesModels;
}
