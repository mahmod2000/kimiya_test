<?php

namespace App\Domains\User\Events;

use Illuminate\Queue\SerializesModels;

class UserLoggedIn
{
    use SerializesModels;
}
